FROM ubuntu:22.04

# Set a non-interactive shell environment
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies and other necessary packages
RUN apt-get update && apt-get install -y \
    cmake \
    make \
    g++ \
    libboost-dev \
    git \
    && rm -rf /var/lib/apt/lists/*

# Copy the repository into the Docker image
WORKDIR /opt
COPY . /opt/Exact

# Create a release build
WORKDIR /opt/Exact
RUN mkdir -p build && cd build \
    && cmake .. -DCMAKE_BUILD_TYPE=Release \
    && make

# Optionally, create a debug build (if needed uncomment the following lines)
# RUN mkdir -p build_debug && cd build_debug \
#     && cmake .. -DCMAKE_BUILD_TYPE=Debug \
#     && make

# Set the default command or entrypoint to run the solver
CMD ["/opt/Exact/build/Exact"]

# Uncomment the following line if debug build is also needed
# CMD ["/opt/Exact/build_debug/Exact"]

